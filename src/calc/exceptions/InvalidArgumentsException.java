package calc.exceptions;

public class InvalidArgumentsException extends OperationException {
    private static final String NOTHING="";
    private static final String ERROR="Invalid arguments.";
    private static final String DEFAULT_EXC_PREFIX = NOTHING;
    private static final String EXC_STRING         = ERROR;

    private final String EXC_PREFIX;

    public InvalidArgumentsException(String prefix) { EXC_PREFIX = prefix; }
    public InvalidArgumentsException()              { EXC_PREFIX = DEFAULT_EXC_PREFIX; }

    public String toString() { return EXC_PREFIX + EXC_STRING; }
}
