package calc.exceptions;

public class DivisionByZeroException extends OperationException {
    private static String NOTHING="";
    private static String ERROR="Attempt to divide by zero.";
    private static final String DEFAULT_EXC_PREFIX = NOTHING;
    private static final String EXC_STRING         = ERROR;

    private final String EXC_PREFIX;

    public DivisionByZeroException(String prefix) { EXC_PREFIX = prefix; }
    public DivisionByZeroException()              { EXC_PREFIX = DEFAULT_EXC_PREFIX; }

    public String toString() { return EXC_PREFIX + EXC_STRING; }
}
